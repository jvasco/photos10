package controller;
/**
 * @author Jordy Vasco
 * @author Nicholas Lelchitsky
 */


import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.Photos;
import model.User;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginController.
 */
public class LoginController {
	
	/** The temp 2. */
	User temp2;
	
	/** The login. */
	@FXML
	private Button login;

	/** The username. */
	@FXML
	private TextField username;

	/** The uname. */
	String uname;
	
	/** The in list. */
	boolean inList = false;

	/**
	 * Handle login button action.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@FXML
	protected void handleLoginButtonAction(ActionEvent event) throws IOException {

		
		Parent parent;
		uname = username.getText();
		User temp = new User(uname);
		
		for(User use: Photos.users)
		{
			//assuming this isn't case sensitive
			if((use.getName().toLowerCase()).equals(uname.toLowerCase()))
			{
				inList = true;
				temp2 = use;
			}
		}
		if (uname.equals("admin")) {
			//System.out.println("OHOU");
			// display admin screen
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AdminScreen.fxml"));
			parent = (Parent) loader.load();
			AdminScreenController controller = loader.getController();

			Scene scene = new Scene(parent);
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			controller.start();
			stage.setScene(scene);
			stage.show();

		}
		
		else if (inList) {
			//System.out.println("user exists");
			/**
			 * display user screen
			 */
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserScreen.fxml"));
			parent = (Parent) loader.load();
			UserScreenController controller = loader.getController();

			Scene scene = new Scene(parent);
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			controller.start(temp2);
			stage.setScene(scene);
			stage.show();
		} else if (uname.length() > 0) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("User does not exist");
			alert.showAndWait();
			return;
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Enter user");
			alert.showAndWait();
			return;
		}
	}

}
