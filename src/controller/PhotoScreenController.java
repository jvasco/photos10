package controller;
/**
 * @author Jordy Vasco
 * @author Nicholas Lelchitsky
 */


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Album;
import model.Photos;
import model.Photo;
import model.Tag;
import model.User;


// TODO: Auto-generated Javadoc
/**
 * The Class PhotoScreenController.
 */

public class PhotoScreenController {

	/** The edit caption. */
	@FXML
	private Button editCaption;

	/** The save caption. */
	@FXML
	private Button saveCaption;

	/** The back. */
	@FXML
	private Button back;

	/** The next. */
	@FXML
	private Button next;

	/** The prev. */
	@FXML
	private Button prev;

	/** The add tag. */
	@FXML
	private Button addTag;

	/** The edit tag. */
	@FXML
	private Button editTag;

	/** The save tag. */
	@FXML
	private Button saveTag;

	/** The movecopy. */
	@FXML
	private Button movecopy;

	/** The cancel. */
	@FXML
	private Button cancel;

	/** The caption. */
	@FXML
	private TextField caption;

	/** The tag name. */
	@FXML
	private TextField tagName;

	/** The tag value. */
	@FXML
	private TextField tagValue;

	/** The tag table. */
	@FXML
	private TableView<Tag> tagTable = new TableView<>();

	/** The table name. */
	@FXML
	private TableColumn<String, Tag> tableName;

	/** The table value. */
	@FXML
	private TableColumn<String, Tag> tableValue;
	
	/** The date. */
	@FXML
	private Label date;

	/** The photo. */
	@FXML
	private ImageView photo;
	
	/** The obs list. */
	private ObservableList<Tag> obsList = FXCollections.observableArrayList();
	
	/** The selected tag. */
	Tag selectedTag;
	
	/** The curr photo. */
	Photo currPhoto;
	
	/** The curr album. */
	Album currAlbum;
	
	/** The edit tag mode. */
	boolean editTagMode = false;
	
	/** The temp value. */
	String tempName, tempValue;
	
	/**
	 * Start.
	 *
	 * @param a the a
	 * @param p the p
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void start(Album a,Photo p) throws IOException {
		currPhoto = p;
		currAlbum = a;
		
		SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
		String fdate = format1.format(p.date.getTime());
		date.setText("Captured on " + fdate);
		
		tagTable.setItems(FXCollections.observableArrayList(obsList));

		tagTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tag>() {
			@Override
			public void changed(ObservableValue<? extends Tag> observable, Tag oldValue, Tag newValue) {
				
				setSelected(newValue);
			}
		});
		tableName.setCellValueFactory(new PropertyValueFactory<>("name"));
		tableValue.setCellValueFactory(new PropertyValueFactory<>("value"));
		obsList.addAll(p.tags);
		tagTable.setItems(FXCollections.observableArrayList(obsList));
		tagTable.getSelectionModel().select(0);
		
		Image image = new Image(p.fp.toURI().toString());
		photo.setImage(image);
		
		caption.setText(p.caption);
		caption.setDisable(true);
		tagName.setDisable(true);
		tagValue.setDisable(true);
		saveTag.setDisable(true);
		saveCaption.setDisable(true);
		cancel.setDisable(true);
	}
	
	/**
	 * Sets the selected.
	 *
	 * @param tag the new selected
	 */
	protected void setSelected(Tag tag) {
		if (tag == null) {
			return;
		}
		selectedTag = tag;
		tagName.setText(tag.getName());
		tagValue.setText(tag.getValue());

	}

	/**
	 * Back.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void back(ActionEvent event) throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumScreen.fxml"));
		Parent parent = (Parent) loader.load();
		AlbumController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(currAlbum, Photos.currUser);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Prev.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void prev(ActionEvent event) throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoScreen.fxml"));
		Parent parent = (Parent) loader.load();
		PhotoScreenController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		int ind = Photos.currUser.albums.get(Photos.currUser.albums.indexOf(currAlbum)).photos.indexOf(currPhoto);
		controller.start(currAlbum, Photos.currUser.albums.get(Photos.currUser.albums.indexOf(currAlbum)).photos.get(ind-1));
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Next.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void next(ActionEvent event) throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoScreen.fxml"));
		Parent parent = (Parent) loader.load();
		PhotoScreenController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		int ind = Photos.currUser.albums.get(Photos.currUser.albums.indexOf(currAlbum)).photos.indexOf(currPhoto);
		controller.start(currAlbum, Photos.currUser.albums.get(Photos.currUser.albums.indexOf(currAlbum)).photos.get(ind+1));
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Movecopy.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void movecopy(ActionEvent event) throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MoveCopyScreen.fxml"));
		Parent parent = (Parent) loader.load();
		MoveCopyController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(currAlbum, currPhoto);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Edits the caption.
	 */
	public void editCaption(){
		caption.setDisable(false);
		saveCaption.setDisable(false);
		editCaption.setDisable(true);
		cancel.setDisable(false);
	}

	/**
	 * Save caption.
	 */
	public void saveCaption(){
		currPhoto.addCaption(caption.getText());
		
		String filename = "users.ser";
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		
		int userind = Photos.users.indexOf(Photos.currUser);
		int albumind = Photos.users.get(userind).albums.indexOf(currAlbum);
		int photoind = Photos.users.get(userind).albums.get(albumind).photos.indexOf(currPhoto);
		
		Photos.users.get(userind).albums.get(albumind).photos.get(photoind).caption = caption.getText();
		
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(Photos.users);

			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		caption.setDisable(true);
		saveCaption.setDisable(true);
		editCaption.setDisable(false);
		cancel.setDisable(true);
	}

	/**
	 * Edits the tag.
	 */
	public void editTag(){
		editTagMode = true;
		
		tempName = tagName.getText();
		tempValue = tagValue.getText();
		
		tagName.setDisable(false);
		tagValue.setDisable(false);
		editTag.setDisable(true);
		addTag.setDisable(true);
		cancel.setDisable(false);
		saveTag.setDisable(false);
	}

	/**
	 * Save tag.
	 */
	public void saveTag(){
		if(tagName.getText().length() == 0 || tagValue.getText().length() == 0){
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please enter both name and value");
			alert.showAndWait();
			return;
		}
		if (editTagMode) {
			if(!obsList.contains(new Tag(tagName.getText(), tagValue.getText())) || (tagName.getText().equals(tempName) && tagValue.getText().equals(tempValue))){
				obsList.get(obsList.indexOf(selectedTag)).setTag(tagName.getText(), tagValue.getText());
			}else{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Tag pair already exists");
				alert.showAndWait();
				return;
			}
			
			tagTable.refresh();
			editTagMode = false;
		}else if(!obsList.contains(new Tag(tagName.getText(),tagValue.getText()))){
			obsList.add(new Tag(tagName.getText(), tagValue.getText()));
			tagTable.setItems(obsList);
		}else{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Tag already exists");
			alert.showAndWait();
			return;
		}
		tagName.setDisable(true);
		tagValue.setDisable(true);
		editTag.setDisable(false);
		addTag.setDisable(false);
		saveTag.setDisable(true);
		cancel.setDisable(true);
		
		String filename = "users.ser";
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		
		int userind = Photos.users.indexOf(Photos.currUser);
		int albumind = Photos.users.get(userind).albums.indexOf(currAlbum);
		int photoind = Photos.users.get(userind).albums.get(albumind).photos.indexOf(currPhoto);
		
		Photos.users.get(userind).albums.get(albumind).photos.get(photoind).tags = new ArrayList<Tag>(obsList);
		
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(Photos.users);

			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Adds the tag.
	 */
	public void addTag(){
		tagName.setDisable(false);
		tagValue.setDisable(false);
		editTag.setDisable(true);
		addTag.setDisable(true);
		saveTag.setDisable(false);
		cancel.setDisable(false);
	}

	/**
	 * Cancel.
	 */
	public void cancel(){
		editTagMode = false;
		
		tagName.setDisable(true);
		tagValue.setDisable(true);
		caption.setDisable(true);
		editTag.setDisable(false);
		addTag.setDisable(false);
		editCaption.setDisable(false);
		saveCaption.setDisable(true);
		cancel.setDisable(true);
		saveTag.setDisable(true);
	}

	

}
