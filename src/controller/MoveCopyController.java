package controller;
/**
 * @author Jordy Vasco
 * @author Nicholas Lelchitsky
 */


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Album;
import model.Photos;
import model.Photo;
import model.Tag;

// TODO: Auto-generated Javadoc
/**
 * The Class MoveCopyController.
 */
public class MoveCopyController {
	
	/** The move to. */
	@FXML
	private Button moveTo;
	
	/** The copy to. */
	@FXML
	private Button copyTo;
	
	/** The back. */
	@FXML
	private Button back;
	
	/** The photo. */
	@FXML
	private ImageView photo;
	
	/** The alabel. */
	@FXML
	private Label alabel;
	
	/** The album table. */
	@FXML
	private TableView<Album> albumTable = new TableView<>();

	/** The album name. */
	@FXML
	private TableColumn<String, Album> albumName;
	
	/** The obs list. */
	private ObservableList<Album> obsList = FXCollections.observableArrayList();
	
	/** The curr album. */
	Album selectedAlbum, currAlbum;
	
	/** The curr photo. */
	Photo currPhoto;
	
	/**
	 * Start.
	 *
	 * @param a the a
	 * @param p the p
	 */
	public void start(Album a, Photo p){
		currAlbum = a;
		currPhoto = p;
		
		albumTable.setItems(FXCollections.observableArrayList(obsList));

		albumTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Album>() {
			@Override
			public void changed(ObservableValue<? extends Album> observable, Album oldValue, Album newValue) {
				setSelected(newValue);
			}
		});
		albumName.setCellValueFactory(new PropertyValueFactory<>("name"));
		obsList.addAll(Photos.currUser.getAlbums());
		obsList.remove(a);
		albumTable.setItems(FXCollections.observableArrayList(obsList));
		albumTable.getSelectionModel().select(0);
		
		Image image = new Image(p.fp.toURI().toString());
		photo.setImage(image);
	}
	
	/**
	 * Sets the selected.
	 *
	 * @param album the new selected
	 */
	protected void setSelected(Album album) {
		if (album == null) {
			return;
		}
		selectedAlbum = album;
		alabel.setText("Destination Album: " + album.getName());

	}
	
	/**
	 * Move to.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void moveTo(ActionEvent event) throws IOException{
		int userind = Photos.users.indexOf(Photos.currUser);
		int destind = Photos.users.get(userind).albums.indexOf(selectedAlbum);
		int currind = Photos.users.get(userind).albums.indexOf(currAlbum);
		
		Photos.users.get(userind).albums.get(destind).photos.add(currPhoto);
		Photos.users.get(userind).albums.get(currind).photos.remove(currPhoto);
		
		String filename = "users.ser";
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(Photos.users);

			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumScreen.fxml"));
		Parent parent = (Parent) loader.load();
		AlbumController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(Photos.users.get(userind).albums.get(currind),Photos.currUser);
		stage.setScene(scene);
		stage.show();
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
		alert.setHeaderText(null);
		alert.setContentText("Photo successfully moved");
		alert.showAndWait();
	}
	
	/**
	 * Copy to.
	 */
	public void copyTo(){
		int userind = Photos.users.indexOf(Photos.currUser);
		int destind = Photos.users.get(userind).albums.indexOf(selectedAlbum);
		
		Photos.users.get(userind).albums.get(destind).photos.add(currPhoto);
		
		String filename = "users.ser";
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(Photos.users);

			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
		alert.setHeaderText(null);
		alert.setContentText("Photo successfully copied");
		alert.showAndWait();
	}
	
	/**
	 * Back.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void back(ActionEvent event) throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoScreen.fxml"));
		Parent parent = (Parent) loader.load();
		PhotoScreenController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(currAlbum, currPhoto);
		stage.setScene(scene);
		stage.show();
	}
	
}
