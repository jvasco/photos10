package controller;
/**
 * @author Jordy Vasco
 * @author Nicholas Lelchitsky
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Album;
import model.Photos;
import model.Photo;
import model.Tag;
import model.User;

// TODO: Auto-generated Javadoc
/**
 * The Class SearchController.
 */

public class SearchController {

	/** The search. */
	@FXML
	private Button search;

	/** The search field. */
	@FXML
	private TextField searchField;

	/** The from date. */
	@FXML
	private DatePicker fromDate;

	/** The to date. */
	@FXML
	private DatePicker toDate;

	/** The create album. */
	@FXML
	private Button createAlbum;

	/** The results. */
	@FXML
	private TableView<Photo> results = new TableView<>();

	/** The photo column. */
	@FXML
	private TableColumn<ImageView, Photo> photoColumn;

	/** The caption column. */
	@FXML
	private TableColumn<String, Photo> captionColumn;

	/** The album column. */

	@FXML
	private TableColumn<String, Photo> albumColumn;

	/** The back. */
	@FXML
	private Button back;

	/** The open. */
	@FXML
	private Button open;

	/** The add tag. */
	@FXML
	private Button addTag;

	/** The delete tag. */

	@FXML
	private Button deleteTag;

	/** The name. */
	@FXML
	private TextField name;

	/** The value. */
	@FXML
	private TextField value;

	/** The album name. */
	@FXML
	private TextField albumName;

	/** The tags table. */
	@FXML
	private TableView<Tag> tagsTable = new TableView<>();

	/** The name column. */
	@FXML
	private TableColumn<String, Tag> nameColumn;

	/** The value column. */
	@FXML
	private TableColumn<String, Tag> valueColumn;

	/** The selected photo. */
	public Photo selectedPhoto;

	/** The selected album. */
	public Album selectedAlbum;

	/** The selected tag. */

	Tag selectedTag;

	/** The from. */
	LocalDate from;

	/** The to. */
	LocalDate to;

	/** The user. */
	User user;

	/** The obs list. */
	private ObservableList<Photo> obsList = FXCollections.observableArrayList();

	/** The tag obs list. */
	private ObservableList<Tag> tagObsList = FXCollections.observableArrayList();

	/**
	 * Start.
	 *
	 * @param currUser
	 *            the curr user
	 */
	public void start(User currUser) {
		// TODO Auto-generated method stub
		user = currUser;
		results.setItems(FXCollections.observableArrayList(obsList));
		results.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Photo>() {

			@Override
			public void changed(ObservableValue<? extends Photo> arg0, Photo arg1, Photo arg2) {
				// TODO Auto-generated method stub
				selectedPhoto = arg2;

			}

		});
		photoColumn.setCellValueFactory(new PropertyValueFactory<>("ImageView"));
		albumColumn.setCellValueFactory(new PropertyValueFactory<>("album"));
		captionColumn.setCellValueFactory(new PropertyValueFactory<>("caption"));

		results.setItems(FXCollections.observableArrayList(obsList));
		results.getSelectionModel().select(0);

		tagsTable.setItems(FXCollections.observableArrayList(tagObsList));
		tagsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tag>() {

			@Override
			public void changed(ObservableValue<? extends Tag> arg0, Tag arg1, Tag arg2) {
				// TODO Auto-generated method stub
				selectedTag = arg2;

			}

		});
		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		valueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

	}

	/**
	 * Search photos.
	 */
	public void searchPhotos() {
		obsList.clear();
		from = fromDate.getValue();
		to = toDate.getValue();
		// System.out.println("What");

		if (from != null && to != null) {

			for (Album album : user.getAlbums()) {
				for (Photo photo : album.getPhotos()) {

					if (photo.isBetweenDates(from, to) && photo.containsAllTags(tagObsList)) {
						photo.setAlbum(album);
						obsList.add(photo);

					}
				}
			}
		} else {
			for (Album album : user.getAlbums()) {
				for (Photo photo : album.getPhotos()) {

					if (photo.containsAllTags(tagObsList)) {
						photo.setAlbum(album);
						obsList.add(photo);
					}
				}
			}
		}
		// System.out.println("Obslist is: " + obsList.toString());
		results.setItems(obsList);
	}

	/**
	 * Open photo.
	 *
	 * @param event
	 *            the event
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void openPhoto(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoScreen.fxml"));
		Parent parent = (Parent) loader.load();
		PhotoScreenController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(null, selectedPhoto);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Adds the tag method.
	 */
	public void addTagMethod() {
		String nameString = name.getText();
		String valueString = value.getText();
		Tag temp = new Tag(nameString, valueString);
		tagObsList.add(temp);
		tagsTable.setItems(tagObsList);
	}

	/**
	 * Delete tag method.
	 */

	public void deleteTagMethod() {
		tagObsList.remove(selectedTag);
		tagsTable.setItems(tagObsList);
	}

	/**
	 * Creates the album method.
	 */

	public void createAlbumMethod() {

		if (obsList.isEmpty()) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("No photos in search results");
			alert.showAndWait();
			return;
		}
		if (albumName.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("No album name");
			alert.showAndWait();
			return;
		}
		Album a = new Album(albumName.getText());
		if (Photos.currUser.getAlbums().contains(a)) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Album already exists");
			alert.showAndWait();
			return;
		}
		for (Photo p : obsList) {
			a.addPhoto(p);
		}

		Photos.users.get(Photos.users.indexOf(Photos.currUser)).addAlbum(a);

		String filename = "users.ser";
		FileOutputStream fos = null;
		ObjectOutputStream out = null;

		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(Photos.users);

			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Success");
		alert.setHeaderText(null);
		alert.setContentText("Album saved");
		alert.showAndWait();
		albumName.setText("");
		return;
	}

	public void backMethod(ActionEvent event) throws IOException {

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserScreen.fxml"));
		Parent parent = (Parent) loader.load();
		UserScreenController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(Photos.currUser);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Open photo method.
	 *
	 * @param event
	 *            the event
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */

	public void openPhotoMethod(ActionEvent event) throws IOException {
		if (selectedPhoto == null) {

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("No photo selected");
			alert.showAndWait();
			return;
		}
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoScreen.fxml"));
		Parent parent = (Parent) loader.load();
		PhotoScreenController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(selectedPhoto.getAlbum(), selectedPhoto);
		stage.setScene(scene);
		stage.show();
	}
}
