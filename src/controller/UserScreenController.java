package controller;
/**
 * @author Jordy Vasco
 * @author Nicholas Lelchitsky
 */



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Album;
import model.Photos;
import model.User;


// TODO: Auto-generated Javadoc
/**
 * The Class UserScreenController.
 */

public class UserScreenController {

	/** The add album. */
	@FXML
	private Button addAlbum;

	/** The rename. */
	@FXML
	private Button rename;

	/** The delete. */
	@FXML
	private Button delete;

	/** The save. */
	@FXML
	private Button save;

	/** The cancel. */
	@FXML
	private Button cancel;

	/** The open album. */
	@FXML
	private Button openAlbum;

	/** The search photos. */
	@FXML
	private Button searchPhotos;

	/** The logout. */
	@FXML
	private Button logout;

	/** The album name text. */
	@FXML
	private TextField albumNameText;

	/** The album table. */
	@FXML
	private TableView<Album> albumTable = new TableView<>();

	/** The album name. */
	@FXML
	private TableColumn<String, Album> albumName;

	/** The album size. */
	@FXML
	private TableColumn<String, Album> albumSize;

	/** The date range. */
	@FXML
	private TableColumn<String, Album> dateRange;

	/** The obs list. */
	private ObservableList<Album> obsList = FXCollections.observableArrayList();
	
	/** The selected album. */
	Album selectedAlbum;
	
	/** The rename mode. */
	boolean renameMode = false;
	
	/** The rename temp. */
	String renameTemp;
	
	/**
	 * Start.
	 *
	 * @param u the u
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void start(User u) throws IOException {
		Photos.currUser = u;

		//System.out.println("Username is: " + u.getName());
		//System.out.println("User albums: " + u.getAlbums().toString());

		albumTable.setItems(FXCollections.observableArrayList(obsList));

		albumTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Album>() {
			@Override
			public void changed(ObservableValue<? extends Album> observable, Album oldValue, Album newValue) {
				save.setDisable(true);
				cancel.setDisable(true);
				setSelected(newValue);
			}
		});
		albumName.setCellValueFactory(new PropertyValueFactory<>("name"));
		albumSize.setCellValueFactory(new PropertyValueFactory<>("size"));
		dateRange.setCellValueFactory(new PropertyValueFactory<>("date"));
		obsList.addAll(u.getAlbums());
		albumTable.setItems(FXCollections.observableArrayList(obsList));
		albumTable.getSelectionModel().select(0);
		albumNameText.setDisable(true);

	}

	/**
	 * Sets the selected.
	 *
	 * @param album the new selected
	 */
	protected void setSelected(Album album) {
		// TODO Auto-generated method stub
		if (album == null) {
			return;
		}
		selectedAlbum = album;
		albumNameText.setText(album.getName());

	}

	/**
	 * Adds the album.
	 */
	public void addAlbum() {
		albumNameText.setDisable(false);
		save.setDisable(false);
		cancel.setDisable(false);
		openAlbum.setDisable(true);
		searchPhotos.setDisable(true);
		addAlbum.setDisable(true);
		rename.setDisable(true);
		delete.setDisable(true);
	}

	/**
	 * Rename.
	 */
	public void rename() {
		renameMode = true;
		renameTemp = albumNameText.getText();

		albumNameText.setDisable(false);
		save.setDisable(false);
		cancel.setDisable(false);
		openAlbum.setDisable(true);
		searchPhotos.setDisable(true);
		addAlbum.setDisable(true);
		rename.setDisable(true);
		delete.setDisable(true);
	}

	/**
	 * Delete album.
	 */
	public void deleteAlbum() {
		Photos.currUser.deleteAlbum(selectedAlbum);
		obsList.remove(obsList.indexOf(selectedAlbum));
		albumTable.setItems(obsList);
	}

	/**
	 * Save name.
	 */
	public void saveName() {

		if (renameMode) {
			if (!obsList.contains(new Album(albumNameText.getText())) || albumNameText.getText().equals(renameTemp)) {
				obsList.get(obsList.indexOf(selectedAlbum)).name = albumNameText.getText();
				//System.out.println("renamed");
			} else {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Album name already exists");
				alert.showAndWait();
				return;
			}

			albumTable.refresh();
			// albumTable.setItems(obsList);

			renameMode = false;
		} else if (!obsList.contains(new Album(albumNameText.getText()))) {
			Photos.currUser.addAlbum(new Album(albumNameText.getText()));
			obsList.add(new Album(albumNameText.getText()));
			albumTable.setItems(obsList);
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Album already exists");
			alert.showAndWait();
			return;
		}

		albumNameText.setDisable(true);
		save.setDisable(true);
		cancel.setDisable(true);
		openAlbum.setDisable(false);
		searchPhotos.setDisable(false);
		addAlbum.setDisable(false);
		rename.setDisable(false);
		delete.setDisable(false);
		
		String filename = "users.ser";
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		Photos.users.get(Photos.users.indexOf(Photos.currUser)).albums = new ArrayList<Album>(obsList);
		
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(Photos.users);

			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// System.out.println("save" + albumNameText.getText() +
		// obsList.toString());
	}

	/**
	 * Cancel.
	 */
	public void cancel() {
		albumNameText.setDisable(true);
		albumNameText.setText(selectedAlbum.name);
		renameMode = false;

		save.setDisable(true);
		cancel.setDisable(true);
		openAlbum.setDisable(false);
		searchPhotos.setDisable(false);
		addAlbum.setDisable(false);
		rename.setDisable(false);
		delete.setDisable(false);
	}

	/**
	 * Logout.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void logout(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginScreen.fxml"));
		Parent parent = (Parent) loader.load();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Open album.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void openAlbum(ActionEvent event) throws IOException {

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumScreen.fxml"));
		Parent parent = (Parent) loader.load();
		AlbumController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(selectedAlbum, Photos.currUser);
		stage.setScene(scene);
		stage.show();

	}

	/**
	 * Search.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void search(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SearchScreen.fxml"));
		Parent parent = (Parent) loader.load();
		SearchController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(Photos.currUser);
		stage.setScene(scene);
		stage.show();
	}

}
