package controller;
/**
 * @author Jordy Vasco
 * @author Nicholas Lelchitsky
 */



import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Optional;

import javafx.application.Application;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.PopupBuilder;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Photos;
import model.Tag;
import model.User;


// TODO: Auto-generated Javadoc
/**
 * The Class AdminScreenController.
 */

public class AdminScreenController {

	/** The logout. */
	@FXML
	private Button logout;

	/** The add new user. */
	@FXML
	private Button addNewUser;

	/** The users table. */
	@FXML
	private TableView<User> usersTable = new TableView<>();
	
	/** The data. */
	/*
	 * @FXML TableColumn<User, String> userName = new
	 * TableColumn<>("usaasderName");
	 * 
	 * @FXML TableColumn<User, User> delete = new TableColumn<>("delete");
	 */
	private final ObservableList<User> data = FXCollections.observableArrayList(Photos.users);

	/** The user name. */
	@FXML
	TableColumn<User, String> userName = new TableColumn<>("userName");

	/** The delete. */
	@FXML
	TableColumn<User, User> delete = new TableColumn<>("delete");

	/**
	 * Start.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	public void start() throws IOException {
		// TableColumn<User, String> userName = new
		// TableColumn<>("usaasderName");
		Stage stage;
		// TableColumn<User, User> delete = new TableColumn<>("delete");

		delete.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		delete.setCellFactory(param -> new TableCell<User, User>() {
			private final Button deleteButton = new Button("Delete");

			@Override
			protected void updateItem(User user, boolean empty) {
				super.updateItem(user, empty);

				if (user == null) {
					setGraphic(null);
					return;
				}
				setGraphic(deleteButton);
				deleteButton.setOnAction(event -> {
					data.remove(user);
					Photos.users.remove(user);

					//System.out.println("Users: " + Photos.users.toString());

					
					String filename = "users.ser";
					FileOutputStream fos = null;
					ObjectOutputStream out = null;
					
					try {
						fos = new FileOutputStream(filename);
						out = new ObjectOutputStream(fos);
						out.writeObject(Photos.users);

						out.close();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				});

			}
		});

		userName.setCellValueFactory(new PropertyValueFactory<>("name"));
		usersTable.setItems(data);
		// usersTable.getColumns().addAll(userName, delete);
		// stage.show();
		usersTable.getSelectionModel().select(0);
	}
	
	/**
	 * Logout.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void logout(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginScreen.fxml"));
		Parent parent = (Parent) loader.load();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Adds the user.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void addUser(ActionEvent event) throws IOException{
		TextInputDialog dialog = new TextInputDialog("User");
		dialog.setTitle("Add New User");
		dialog.setHeaderText("Add New User");
		dialog.setContentText("Username: ");
		
		Optional<String> result = dialog.showAndWait();
		if(result.isPresent()){
			//System.out.println("New user is: " + result.get());
			User nu = new User(result.get());
			data.add(nu);
			Photos.users.add(nu);

			
			String filename = "users.ser";
			FileOutputStream fos = null;
			ObjectOutputStream out = null;
			
			try {
				fos = new FileOutputStream(filename);
				out = new ObjectOutputStream(fos);
				out.writeObject(Photos.users);

				out.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
		
	}
	
	/** The obs list. */
	private ObservableList<User> obsList;

}
