package controller;
/**
 * @author Jordy Vasco
 * @author Nicholas Lelchitsky
 */



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Album;
import model.Photos;
import model.Photo;
import model.User;


// TODO: Auto-generated Javadoc
/**
 * The Class AlbumController.
 */

public class AlbumController {

	/** The album. */
	public Album album;

	/** The photo table. */
	@FXML
	private TableView<Photo> photoTable = new TableView<>();

	/** The photo column. */
	@FXML
	private TableColumn<ImageView, Photo> photoColumn;

	/** The caption column. */
	@FXML
	private TableColumn<String, Photo> captionColumn;

	/** The add photo. */
	@FXML
	private Button addPhoto;

	/** The delete photo. */
	@FXML
	private Button deletePhoto;

	/** The view photo. */
	@FXML
	private Button viewPhoto;

	/** The back. */
	@FXML
	private Button back;

	/** The logout. */
	@FXML
	private Button logout;

	/** The album name. */
	@FXML
	private Text albumName;

	/** The selected photo. */
	Photo selectedPhoto;

	/** The obs list. */
	private ObservableList<Photo> obsList = FXCollections.observableArrayList();

	/**
	 * Start.
	 *
	 * @param a the a
	 * @param u the u
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void start(Album a, User u) throws IOException {
		albumName.setText(a.getName());
		album = a;
		Photos.currUser = u;
		photoTable.setItems(FXCollections.observableArrayList(obsList));
		//System.out.println("Album name is: " + a.getName());

		photoTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Photo>() {

			@Override
			public void changed(ObservableValue<? extends Photo> arg0, Photo arg1, Photo arg2) {
				// TODO Auto-generated method stub
				setSelected(arg2);

			}

		});
		photoColumn.setPrefWidth(75);
		photoColumn.setCellValueFactory(new PropertyValueFactory<>("ImageView"));

		captionColumn.setCellValueFactory(new PropertyValueFactory<>("caption"));

		obsList.addAll(a.getPhotos());
		photoTable.setItems(FXCollections.observableArrayList(obsList));
		photoTable.getSelectionModel().select(0);

		//System.out.println("Photo names are: " + a.getPhotos().toString());
	}

	/**
	 * Adds the photo method.
	 */
	public void addPhotoMethod() {

		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Add Image");
		FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
		FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
		fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
		File selectedFile = fileChooser.showOpenDialog(null);
		Photo temp = new Photo(selectedFile);

		album.addPhoto(temp);
		obsList.add(temp);
		photoTable.setItems(obsList);
		
		String filename = "users.ser";
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		
		int userind = Photos.users.indexOf(Photos.currUser);
		int albumind = Photos.users.get(userind).albums.indexOf(album);
		
		Photos.users.get(userind).albums.get(albumind).photos = new ArrayList<Photo>(obsList);
		
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(Photos.users);

			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Delete photo method.
	 */
	public void deletePhotoMethod() {
		//System.out.println("Albums: " + album.getPhotos().toString());
		album.deletePhoto(selectedPhoto);
		//System.out.println("Albums after delete: " + album.getPhotos().toString());
		
		obsList.remove(obsList.indexOf(selectedPhoto));
		photoTable.setItems(obsList);
		
		String filename = "users.ser";
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		
		int userind = Photos.users.indexOf(Photos.currUser);
		int albumind = Photos.users.get(userind).albums.indexOf(album);
		
		Photos.users.get(userind).albums.get(albumind).photos = new ArrayList<Photo>(obsList);
		
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(Photos.users);

			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * View photo method.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void viewPhotoMethod(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoScreen.fxml"));
		Parent parent = (Parent) loader.load();
		PhotoScreenController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(album, selectedPhoto);
		stage.setScene(scene);
		stage.show();

	}

	/**
	 * Sets the selected.
	 *
	 * @param photo the new selected
	 */
	protected void setSelected(Photo photo) {
		if (photo == null) {
			return;
		}
		selectedPhoto = photo;
	}

	/**
	 * Back method.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void backMethod(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserScreen.fxml"));
		Parent parent = (Parent) loader.load();
		UserScreenController controller = loader.getController();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		controller.start(Photos.currUser);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Logout method.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void logoutMethod(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginScreen.fxml"));
		Parent parent = (Parent) loader.load();
		Scene scene = new Scene(parent);
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setScene(scene);
		stage.show();
	}

}
