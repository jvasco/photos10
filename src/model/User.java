package model;


/**
 * @author Nicholas Lelchitsky
 * @author Jordy Vasco
 */


import java.io.Serializable;
import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
public class User implements Serializable {
	
	/** The name. */
	String name;
	
	/** The albums. */
	public ArrayList<Album> albums = new ArrayList<Album>();

	/**
	 * Instantiates a new user.
	 *
	 * @param name the name
	 */
	public User(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the albums.
	 *
	 * @return the albums
	 */
	public ArrayList<Album> getAlbums(){
		return albums;
	}
	
	/**
	 * Adds the album.
	 *
	 * @param a the a
	 * @return true, if successful
	 */
	public boolean addAlbum(Album a) {
		if (!albums.contains(a)) {
			albums.add(a);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Delete album.
	 *
	 * @param a the a
	 */
	public void deleteAlbum(Album a) {
		albums.remove(a);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			User u = (User) object;
			if (this.name.toLowerCase().equals(u.name.toLowerCase())) {
				result = true;
			}
		}
		return result;
	}
}
