package model;


/**
 * @author Nicholas Lelchitsky
 * @author Jordy Vasco
 */


import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

// TODO: Auto-generated Javadoc
/**
 * The Class Photo.
 */
public class Photo implements Comparable<Photo>, Serializable {
	
	/** The fp. */
	public File fp;
	
	/** The caption. */
	public String caption = "";
	
	/** The date. */
	public Calendar date;
	
	/** The tags. */
	public ArrayList<Tag> tags = new ArrayList<Tag>();
	
	/** The album. */
	Album album;

	/**
	 * Instantiates a new photo.
	 *
	 * @param fp the fp
	 */
	public Photo(File fp) {
		this.fp = fp;
		Date d = new Date(fp.lastModified());
		date = Calendar.getInstance();
		date.setTime(d);
		date.set(Calendar.MILLISECOND, 0);
		//image = new Image(fp.toString());
	}

	/**
	 * Adds the tag.
	 *
	 * @param tag the tag
	 */
	public void addTag(Tag tag) {
		tags.add(tag);
	}

	/**
	 * Delete tag.
	 *
	 * @param tag the tag
	 */
	public void deleteTag(Tag tag) {
		tags.remove(tag);
	}

	/**
	 * Adds the caption.
	 *
	 * @param caption the caption
	 */
	public void addCaption(String caption) {
		this.caption = caption;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yy");
		return format1.format(date.getTime());
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Photo p) {
		return date.compareTo(p.date);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			Photo p = (Photo) object;
			if (this.fp.equals(p.fp)) {
				result = true;
			}
		}
		return result;
}
	
	/**
	 * Gets the fp.
	 *
	 * @return the fp
	 */
	public File getfp(){
		return fp;
	}

	/**
	 * Gets the caption.
	 *
	 * @return the caption
	 */
	public String getCaption(){
		return caption;
	}
	
	/**
	 * Gets the image view.
	 *
	 * @return the image view
	 */
	public ImageView getImageView(){
		//System.out.println("Getting Imageview");
		ImageView imageview = new ImageView(this.fp.toURI().toString());
		imageview.setFitHeight(75);
		imageview.setFitWidth(75);
		return imageview;
	}
	
	/**
	 * Checks if is between dates.
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return true, if is between dates
	 */
	public boolean isBetweenDates(LocalDate fromDate, LocalDate toDate){
		LocalDate d = date.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		
		if(d.isAfter(fromDate) && d.isBefore(toDate) || d.equals(fromDate) || d.equals(toDate)){
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public ArrayList<Tag> getTags(){
		return tags;
	}
	
	/**
	 * Contains all tags.
	 *
	 * @param tagObsList the tag obs list
	 * @return true, if successful
	 */
	public boolean containsAllTags(ObservableList<Tag> tagObsList){
		if(tagObsList.isEmpty()){
			return true;
		}
		if(tags.isEmpty()){
			return false;
		}
		return tags.containsAll(tagObsList);
	}
	
	/**
	 * Sets the album.
	 *
	 * @param a the new album
	 */
	public void setAlbum(Album a){
		album = a;
	}
	
	/**
	 * Gets the album.
	 *
	 * @return the album
	 */
	public Album getAlbum(){
		return album;
	}
}
