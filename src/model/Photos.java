package model;

/**
 * @author Nicholas Lelchitsky
 * @author Jordy Vasco
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class Photos.
 */
public class Photos extends Application {
	
	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/LoginScreen.fxml"));

		Pane root = (Pane) loader.load();

		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	/** The users. */
	public static ArrayList<User> users = new ArrayList<User>();

	/** The curr user. */
	public static User currUser;
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		//ArrayList<User> users = new ArrayList<User>();
		String filename = "users.ser";
		/**
		 * stock user
		 */
		User stock = new User("stock");
		Album s = new Album("stock");
		
		Photo s1 = new Photo(new File("stock/stock1.jpg"));
		Photo s2 = new Photo(new File("stock/stock2.jpg"));
		Photo s3 = new Photo(new File("stock/stock3.jpg"));
		s1.caption="Hello";
		s2.caption="World";
		s3.caption="Test";
		s.addPhoto(s1);
		s.addPhoto(s2);
		s.addPhoto(s3);
		stock.addAlbum(s);
		Album t = new Album("stock2");
		Photo s4 = new Photo(new File("stock/stock1.jpg"));
		t.addPhoto(s4);
		stock.addAlbum(t);
		users.add(stock);

		// test
//		User u = new User("Nick");
//		User j = new User("Jordy");
//		Album a = new Album("NickAlbum");
//		Album b = new Album("JordyAlbum");
//		Photo p = new Photo(new File("stock/stock1.jpg"));
//		p.addTag(new Tag("banana","yellow"));
//		p.addTag(new Tag("banana","pink"));
//		p.addCaption("testcaption");
//		a.addPhoto(p);
//		b.addPhoto(p);
//		u.addAlbum(a);
//		j.addAlbum(b);
//		
//		users.add(u);
//		users.add(j);

		/**
		 * save object to file
		 */
//		FileOutputStream fos = null;
//		ObjectOutputStream out = null;
//		try {
//			fos = new FileOutputStream(filename);
//			out = new ObjectOutputStream(fos);
//			out.writeObject(users);
//
//			out.close();
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
		/**
		 * read object from file
		 */
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(filename);
			in = new ObjectInputStream(fis);
			users = (ArrayList) in.readObject();
			in.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		ObservableList<User> obusers = FXCollections.observableList(users);

//		for (User i : obusers) {
//			System.out.println(i.name + " " + i.albums.get(0).name + " " + i.albums.get(0).photos.get(0).fp + " "
//					+ i.albums.get(0).photos.get(0).getDate());
//		}

		launch(args);
	}
}
